package com.epam.factory;

public enum CopterServiceType {
    SOAP,
    REST
}
